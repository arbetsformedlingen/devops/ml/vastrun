DOWNLOADDIR = /tmp/dl4
INSTANCE_ID ?= 7834851 # update if you need it
#PROJ = ../process-jobads-with-gpt-sw3
PROJ = ../stable-diffusion

create: venv
	. venv/bin/activate \
	&& python src/vastrun.py create --proj_dir $(PROJ)

full: venv
	. venv/bin/activate \
	&& python src/vastrun.py full --proj_dir $(PROJ) --poll_download_dir $(DOWNLOADDIR)

show: venv
	. venv/bin/activate \
	&& python src/vastrun.py show --instance_id $(INSTANCE_ID)

poll: venv
	. venv/bin/activate \
	&& python src/vastrun.py poll --instance_id $(INSTANCE_ID) --proj_dir $(PROJ) --poll_download_dir $(DOWNLOADDIR)

destroy: venv
	. venv/bin/activate \
	&& python src/vastrun.py destroy --instance_id $(INSTANCE_ID)

install: venv

venv:
	python3 -m venv venv \
	&& . venv/bin/activate \
	&& pip install -r requirements.txt
