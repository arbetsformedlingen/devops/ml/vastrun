# VAST Operations

This Python script provides a command-line interface for performing various operations on VAST instances. The operations include creating, searching, destroying, and showing instances, as well as polling for results.


## Requirements

- Python 3.10 or higher
- [vast.io cli](https://cloud.vast.ai/cli/)

## Installation
```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```
TODO: add alternative installation method for the dependencies (apt/dnf/brew/choco).


## Usage

The script is run from the command line with the operation to perform as the first argument. The available operations are:

- `create`: Create a new VAST instance.
- `search`: Search for VAST offers.
- `destroy`: Destroy a VAST instance.
- `show`: Show information about a VAST instance.
- `poll`: Poll for results from a VAST instance.
- `full`: Perform a full operation cycle (create, poll, destroy).

Additional arguments may be required depending on the operation. Use the `-h` or `--help` option to see the available arguments:

Specify your vast.io ssh key with `--ssh_key <your vast ssh key>`.


```bash
. venv/bin/activate
python src/vastrun.py -h
```

Please note that to use operations `poll` and `full`, you need to execute the program in an [ssh-agent](https://man.openbsd.org/ssh-agent) environment,
so after `. venv/bin/activate` you should do `ssh-agent bash && ssh-add <your vast ssh key>` before running the program.


## Example

To create a new VAST instance with a specific project directory and price (optional):

```bash
python src/vastrun.py create --proj_dir /path/to/project --price 0.10
```

To destroy a VAST instance with a specific instance ID:

```bash
python src/vastrun.py destroy --instance_id 123456
```

## Project Settings

The script reads project settings from a `.vastrun.json` file in the project directory. The settings include:

- `gpu_ram`: The GPU RAM requirement.
- `cpu_ram`: The CPU RAM requirement.
- `disk_space`: The disk space requirement.
- `num_gpus`: The number of GPUs requirement.
- `driver_version`: The driver version requirement.
- `label`: The label for the VAST instance.
- `image`: The Docker image to use.
- `start_script`: The start script to run on the VAST instance.
- `finish_semaphore`: The semaphore file to indicate the finish of the instance.
- `result_dir`: The directory to store the results.

If not provided, default values will be used.

See [here](https://gitlab.com/arbetsformedlingen/devops/ml/process-jobads-with-gpt-sw3) for an example project which can be run with `vastrun`.

## Logging

The script logs information and error messages to stdout. The log messages include the commands executed and their output, as well as any exceptions that occur.

## Cleanup

The script cleans up temporary directories and files it creates during its operation. This includes the temporary directory created when cloning a Git repository.

## Note

This script is a basic implementation and does not handle all edge cases. You may need to modify it to suit your specific needs.

## LICENSE
Apache 2.0
