import argparse
import json
import tempfile
import time

import git
import sys
import logging
import subprocess
import os
import random


default_gpu_ram = 12
default_cpu_ram = 8
default_disk_space = 40
default_num_gpus = 1
default_driver_version = '525.105.17'
default_image = "pytorch/pytorch:latest"
default_start_script = "start.sh"

vast_ssh_key = os.path.expanduser("~/.ssh/config.d.priv/keys/vast")

# init logger to stdout
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

def execute_command(cmds):
    # Create a list to hold the subprocesses
    processes = []

    # cmds is a list of list of strings. clear it of empty strings
    cmds = [[str(x) for x in cmd if x] for cmd in cmds]

    # print cmds as a string
    for cmd in cmds:
        logging.info(' '.join(cmd))

    # Create the first subprocess
    processes.append(subprocess.Popen(map(str, cmds[0]), stdout=subprocess.PIPE, stderr=subprocess.PIPE))

    # Create the rest of the subprocesses, each one connected to the previous one
    for cmd in cmds[1:]:
        processes.append(subprocess.Popen(map(str, cmd), stdin=processes[-1].stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE))

        # Allow previous process to receive a SIGPIPE if process exits
        processes[-2].stdout.close()

    # Wait for the last process to finish and get stdout, stderr, and return code
    stdout, stderr = processes[-1].communicate()
    return_code = processes[-1].returncode

    # Check if the return code is non-zero (indicating an error)
    if return_code != 0:
        logging.error(f"Command failed with return code {return_code}: {stderr.decode('utf-8').strip()}")

    return return_code, stdout.decode('utf-8').strip()


def read_proj_settings(proj_dir=None):
    with open(f'{proj_dir}/.vastrun.json', 'r') as file:
        settings = json.load(file)
    if not settings.get('gpu_ram'):
        settings['gpu_ram'] = default_gpu_ram
    if not settings.get('cpu_ram'):
        settings['cpu_ram'] = default_cpu_ram
    if not settings.get('disk_space'):
        settings['disk_space'] = default_disk_space
    if not settings.get('num_gpus'):
        settings['num_gpus'] = default_num_gpus
    if not settings.get('driver_version'):
        settings['driver_version'] = default_driver_version
    if not settings.get('label'):
        settings['label'] = f'{os.path.basename(proj_dir)}_{random.randint(10000000, 99999999)}'
    if not settings.get('image'):
        settings['image'] = default_image
    if not settings.get('start_script'):
        settings['start_script'] = default_start_script
    settings['start_script'] = f"{proj_dir}/{settings['start_script']}"

    if not os.path.isfile(f'{proj_dir}/{settings["start_script"]}'):
        raise Exception(f"The file {proj_dir}/{settings['start_script']} does not exist.")

    return settings


def create(settings, price=None):
    offer = search_offers(proj_settings)[0]
    command = [ "vast", "create", "instance", offer['id'], "--image", f"docker.io/{settings['image']}",
        "--label", settings['label'], "--onstart", settings['start_script'],
        "--ssh", "--disk", settings['disk_space'], ('--price ' + price if price else '') ]
    _, output = execute_command([command])
    start_index = output.find('{')
    output = output[start_index:].replace("'", '"').replace('True', 'true').replace('False', 'false')

    try:
        order = json.loads(output)
    except Exception as e:
        raise Exception(f"Failed to parse the output: {output}")

    if 'success' not in order or order['success'] is not True:
        raise Exception("The 'success' field is missing or not True.")

    # Check the 'new_contract' field
    if 'new_contract' not in order or not isinstance(order['new_contract'], int):
        raise Exception("The 'new_contract' field is missing or not an integer.")

    return order['new_contract']


def search_offers(settings=None):
    def version_compare(a, b):
        a_parts = list(map(int, a.split('.')))
        b_parts = list(map(int, b.split('.')))

        if a_parts[0] > b_parts[0]:
            return 1
        elif a_parts[0] < b_parts[0]:
            return -1

        if a_parts[1] > b_parts[1]:
            return 1
        elif a_parts[1] < b_parts[1]:
            return -1

        if a_parts[2] > b_parts[2]:
            return 1
        elif a_parts[2] < b_parts[2]:
            return -1

        return 0

    command = ["vast", "search", "offers", "--raw", "--type", "bid", "--order", "dph_total,dlperf_per_dphtotal",
        f"num_gpus>={settings['num_gpus']} disk_space>={settings['disk_space']} \
        gpu_ram>={settings['gpu_ram']} cpu_ram>={settings['cpu_ram']}" ]

    _, output = execute_command([command])
    offers = json.loads(output)

    filtered_offers = [offer for offer in offers if 'driver_version' in offer and version_compare(offer['driver_version'], settings['driver_version']) >= 0]

    return filtered_offers


def get_ssh_connect_string(latest_id):
    # Execute the command and get the output
    command = ["vast", "show", "instances", "--raw"]
    _, output = execute_command([command])
    instances = json.loads(output)

    # Find the instance with the given ID
    latest_id_num = int(latest_id)
    instance = [i for i in instances if i["id"] == latest_id_num][0]

    if instance is None:
        raise Exception(f"No instance found with ID {latest_id}")

    # Get the SSH host and port
    ssh_host = instance['ssh_host']
    ssh_port = instance['ssh_port']

    # Construct the SSH connect string
    ssh_connect_string = ["ssh", "-o", "StrictHostKeyChecking=no", "-p", ssh_port, "-l", "root", "-i", vast_ssh_key, ssh_host ]

    return ssh_connect_string


def poll(instance_id, poll_download_dir, proj_settings):
    ssh_cmd = get_ssh_connect_string(instance_id)
    os.makedirs(poll_download_dir, exist_ok=True)

    finished = False
    while not finished:
        command = ssh_cmd + [ "bash", "-c", f"date ; tail /var/log/onstart.log ; echo; ls {proj_settings['finish_semaphore']}"]
        logging.info(command)
        status, output = execute_command([command])
        logging.info(output)
        finished = status == 0
        time.sleep(10)

    logging.info("Instance finished.")

    # Copy the results
    logging.info("Copying the results...")
    command = ssh_cmd + ["cat", "/var/log/onstart.log"]
    status, output = execute_command([command])
    with open(f"{poll_download_dir}/onstart.log", 'w') as f:
        print(output, file=f)

    tar_cmd = ["tar", "-C", proj_settings['result_dir'], "-zcf", "-", "."]
    status, output = execute_command([ssh_cmd + tar_cmd, ["tar", "-C", poll_download_dir, "-zxvf", "-"]])

    logging.info(f"Results copied, exit status {status}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='VAST operations.')
    parser.add_argument('operation', choices=['create', 'search', 'destroy', 'show', 'poll', 'full'], help='The operation to perform.')
    parser.add_argument('--proj_dir', help='A directory with a project to run in vast.ai.')
    parser.add_argument('--proj_url', help='A git repo url with a project to run in vast.ai.')
    parser.add_argument('--price', help='Make a bid with this price instead of ordering on-demand.')
    parser.add_argument('--poll_download_dir', help='Download results into this directory.')
    parser.add_argument('--instance_id', help='The instance id for destroy and show operations.')
    args = parser.parse_args()

    if args.proj_url and args.proj_dir:
        print("Please provide only one of proj_dir or proj_url.")
        exit(1)

    api_key_file = f'{os.environ["HOME"]}/.vast_api_key'
    if not os.path.isfile(api_key_file) or os.path.getsize(api_key_file) == 0:
        raise Exception(f"The file {api_key_file} does not exist or is empty.")

    temp_dir = None
    try:
        if args.proj_url:
            temp_dir = tempfile.TemporaryDirectory()
            git.Repo.clone_from(args.proj_url, temp_dir.name)
            args.proj_dir = temp_dir.name

        proj_settings = None
        if args.operation in ['create', 'search', 'poll', 'full']:
            if not args.proj_dir:
                raise Exception("Please provide a project directory.")
            proj_settings = read_proj_settings(args.proj_dir)

        if args.operation in ['poll', 'full']:
            if not os.environ.get('SSH_AGENT_PID'):
                raise Exception("SSH_AGENT_PID is not set.")

        if args.operation in ['poll', 'full']:
            if "finish_semaphore" not in proj_settings or "result_dir" not in proj_settings:
                raise Exception("finish_semaphore or result_dir not found in proj_settings.")
            if not args.poll_download_dir:
                raise Exception("Please provide a directory to download the results to.")

        if args.operation in ['destroy', 'show'] and not args.instance_id:
            raise f"Please provide an instance id for {args.operation} operation."

        if args.operation in ['create', 'full']:
            order_id = create(proj_settings, args.price)
            logging.info(f"Order ID: {order_id}")
            logging.info(f"SSH connect string: {get_ssh_connect_string(order_id)}")
            args.instance_id = order_id

        if args.operation in ['poll', 'full']:
            poll(args.instance_id, args.poll_download_dir, proj_settings)

        if args.operation == 'search':
            offers = search_offers(proj_settings)
            print(json.dumps(offers, indent=4))

        if args.operation in ['destroy', 'full']:
            command = ["vast", "destroy", "instance", args.instance_id, "--raw"]
            _, output = execute_command([command])
            print(output)

        if args.operation in ['show']:
            command = ["vast", "show", "instance", args.instance_id, "--raw"]
            _, output = execute_command([command])
            print(output)
            logging.info(f"SSH connect string: {get_ssh_connect_string(args.instance_id)}")

    except Exception as e:
        print(e, file=sys.stderr)
        raise(e)
    finally:
        if temp_dir:
            temp_dir.cleanup()
